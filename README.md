# demoji

dmenu + emoji == demoji

![](https://gitlab.com/ahrs/demoji/raw/master/static/demoji_rofi.png)

Demoji was inspired by [emoji-keyboard](https://github.com/OzymandiasTheGreat/emoji-keyboard) a GTK based virtual keyboard-like emoji picker. I wanted something more lightweight for tiling window managers.

## Installation

Simply, recursively clone the repo some place.

`git clone --recursive https://gitlab.com/ahrs/demoji.git`

## Usage

Add an appropriate key-binding to your window manager e.g, for i3 something like this:

`bindsym $alt+shift+space exec ~/demoji/demoji`
